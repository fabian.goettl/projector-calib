#include "kinectv2stream.cpp"

#define _USE_MATH_DEFINES
#include <cmath>

#ifdef __WIN32__
#include <Windows.h>
#endif
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <assert.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;

#define MARK_DEPTH_POINTS 0

const int nrFrames = 4;

const bool LOAD_IMAGES = false;
const bool STORE_IMAGES = true && !LOAD_IMAGES;

const bool MIRROR_PATTERN = true;

// Option for testing previous intrinsics by clicking in RGB image (LOAD_IMAGES should be false)
const bool LOAD_PREVIOUS_PROJ_CALIB = true;

const bool USE_LOW_RES = false;

const int RGB_COLUMNS = USE_LOW_RES ? 640 : 1920;
const int RGB_ROWS = USE_LOW_RES ? 360 : 1080;

const int PROJ_WIDTH = USE_LOW_RES ? 1024 : 1920;
const int PROJ_HEIGHT = USE_LOW_RES ? 768 : 1080;

const int IR_COLUMNS = 512;
const int IR_ROWS = 424;

const int MAX_DEPTH = 8000; // max sensing range of kinect is 8000 mm 
const int MAX_USHORT_VALUE = 65535;
const int MAX_KINECT_VAL = 52685;


const static string kinectRGBIntrinsics = LOAD_IMAGES ? "storage/kinectv2_rgb.xml" : "kinectv2_rgb.xml";
const static string kinectIRIntrinsics = LOAD_IMAGES ? "storage/kinectv2_ir.xml" : "kinectv2_ir.xml";
const static string kinectStereoExtrinsics = LOAD_IMAGES ? "storage/kinectv2_stereo.xml" : "kinectv2_stereo.xml";
const static string kinectProjectorParams = "kinectv2_projector.xml";


Size boardSize(4, 4);
int squareSize = 100; // default
const int squareSizeMax = 200;

int chessboardOffsetX = 20; // default
int chessboardOffsetY = 20; // default

const int chessboardOffsetMaxX = PROJ_WIDTH - (boardSize.width + 1)*squareSize;
const int chessboardOffsetMaxY = PROJ_HEIGHT - (boardSize.height + 1)*squareSize;

int chessboardBackgroundColor = 150; //default

Mat kinectRGBRaw = Mat(RGB_ROWS, RGB_COLUMNS, CV_8UC4);
Mat kinectDepthRaw = Mat(IR_ROWS, IR_COLUMNS, CV_16UC1);
Mat rgb_d = Mat(IR_ROWS, IR_COLUMNS, CV_8UC4);
Mat pattern = Mat(PROJ_HEIGHT, PROJ_WIDTH, CV_8UC1, chessboardBackgroundColor);

KinectV2Stream ks;

Mat cameraMatrixRGB, distCoeffsRGB;
Mat cameraMatrixIR, distCoeffsIR;
Mat cameraMatrixProj, distCoeffsProj;
Mat rgbToDepthR, rgbToDepthT;
Mat depthToProjR, depthToProjT;

double rgbCX, rgbCY, rgbFX, rgbFY;
double depthCX, depthCY, depthFX, depthFY;

vector<vector<Point3f> > worldPoints;
vector<vector<Point2f> > modelPoints;
vector<vector<Point2f> > imagePoints;

vector<Point2f> mappedRgbPoints;
vector<Point3d> depthWorldCoords;

bool calibrated = false;
bool stereoCalibrated = false;

void mapImg(Mat& rgb, Mat& depth, Mat& rgb_d);
Point3d depthToW(int x, int y, float depth);
Point2i wToRGB(const Vec3d & point);
static bool runProjectorCalibration();
void showCalibrationPattern();
static void onMouse(int event, int x, int y, int, void*);
Point3d findWorldPoint(Point2i rgbPoint);
static void calcBoardCornerPositionsWithOffset(Size boardSize, float squareSize, vector<Point2f>& corners);
static void calcBoardCornerPositionsWithOffsetMirrored(Size boardSize, float squareSize, vector<Point2f>& corners);
static void calcCircleGridPositions(Size boardSize, float squareSize, vector<Point3f>& corners);
void processFrame(Mat &rgb, Mat &d, vector<Point2f> &frameModelPoints);
void storeProjectorCalibAndPoints();
//static bool runProjectorStereoCalibration(Mat& rvec, Mat& tvec, Mat& E, Mat& F);
void qrCalibrate(vector<vector<Point3f>> worldPoints, vector<vector<Point2f>> modelPoints);
Point3d depthWorldtoRgbWorld(Point3d depthWorld);


vector<Mat> nextRGBDImagePair() {
	Mat rgb;
	Mat d;
	vector<Mat> rgbDepth = vector<Mat>();

	ks.getRGBFeed(kinectRGBRaw);
	rgbDepth.push_back(kinectRGBRaw);

	ks.getDepthFrame(kinectDepthRaw);
	rgbDepth.push_back(kinectDepthRaw);

	return rgbDepth;
}


void on_trackbar_offset(int, void*)
{
	showCalibrationPattern();
}

int main(int argc, char* argv[])
{
	FileStorage fsRGB(kinectRGBIntrinsics, FileStorage::READ);
	fsRGB["camera_matrix"] >> cameraMatrixRGB;
	fsRGB["distortion_coefficients"] >> distCoeffsRGB;
	FileStorage fsIR(kinectIRIntrinsics, FileStorage::READ);
	fsIR["camera_matrix"] >> cameraMatrixIR;
	fsIR["distortion_coefficients"] >> distCoeffsIR;
	FileStorage fsStereo(kinectStereoExtrinsics, FileStorage::READ);
	fsStereo["R"] >> rgbToDepthR;
	fsStereo["T"] >> rgbToDepthT;

	if (LOAD_PREVIOUS_PROJ_CALIB) {
		// Load from previous calibration
		FileStorage fsPr(kinectProjectorParams, FileStorage::READ);
		fsPr["camera_matrix"] >> cameraMatrixProj;
		fsPr["distortion_coefficients"] >> distCoeffsProj;
		fsPr["R"] >> depthToProjR;
		fsPr["T"] >> depthToProjT;
	}

	// rgb intrinsics
	rgbFX = cameraMatrixRGB.at<double>(0, 0);
	rgbFY = cameraMatrixRGB.at<double>(1, 1);
	rgbCX = cameraMatrixRGB.at<double>(0, 2);
	rgbCY = cameraMatrixRGB.at<double>(1, 2);

	//depth intrinsics
	depthFX = cameraMatrixIR.at<double>(0, 0);
	depthFY = cameraMatrixIR.at<double>(1, 1);
	depthCX = cameraMatrixIR.at<double>(0, 2);
	depthCY = cameraMatrixIR.at<double>(1, 2);

	namedWindow("RGB");
	namedWindow("RGB-D");
	setMouseCallback("RGB", onMouse, 0);

	if (!LOAD_IMAGES) {
		// Use Kinect to record images

		if (FAILED(ks.kinectSensorIntialise())) {
			std::cout << "No KINECT device found. Aborting" << std::endl;
			exit(-1);
		}

		createTrackbar("CB Size", "RGB-D", &squareSize, squareSizeMax, on_trackbar_offset);
		createTrackbar("X-Offset", "RGB-D", &chessboardOffsetX, chessboardOffsetMaxX, on_trackbar_offset);
		createTrackbar("Y-Offset", "RGB-D", &chessboardOffsetY, chessboardOffsetMaxY, on_trackbar_offset);
		createTrackbar("BackgroundColor", "RGB-D", &chessboardBackgroundColor, 255, on_trackbar_offset);

		showCalibrationPattern();

		while (1)
		{
			vector<Mat> views = nextRGBDImagePair();

			Mat rgb = views.at(0);
			Mat d = views.at(1);

			Mat tempRGB;

			// undistort with intrinsics
			undistort(rgb, tempRGB, cameraMatrixRGB, distCoeffsRGB);
			rgb = tempRGB;

			mapImg(rgb, d, rgb_d);

			char key = (char)waitKey(50);

			if (key == ' ') {
				vector<Point2f> frameModelPoints;

				Mat tmpPattern;

				if (!MIRROR_PATTERN) {
					tmpPattern = pattern;
				}
				else {
					cv::flip(pattern, tmpPattern, 1);
				}
			

				// Find corners in the known model
				bool found = findChessboardCorners(tmpPattern, boardSize, frameModelPoints);
				if (!found) {
					cerr << "Could not detect chessboard corners in model!";
				}

				// Debug preview of found chessboard corners
				drawChessboardCorners(tmpPattern, boardSize, Mat(frameModelPoints), found);
				imshow("Chessboard", tmpPattern);
				waitKey(50);

				if (worldPoints.size() < nrFrames) {
					processFrame(rgb, d, frameModelPoints);
				}
				else if (!calibrated) {
					calibrated = runProjectorCalibration();
				}
				// Remove debug pattern and show original pattern
				imshow("Chessboard", pattern);
			}

			cv::imshow("RGB", rgb);
			cv::imshow("RGB-D", rgb_d);

			if (waitKey(50) == 27) break; // wait for esc
		}
	}
	else {
		// Load images and run calibration
		for (int i = 0; i < nrFrames; i++) {
			ostringstream rgb_filename, d_filename, cb_filename;
			rgb_filename << "storage/rgb" << i << ".png";
			d_filename << "storage/d" << i << ".png";
			cb_filename << "storage/cb" << i << ".png";

			Mat rgb = imread(rgb_filename.str(), CV_LOAD_IMAGE_UNCHANGED);
			Mat d = imread(d_filename.str(), CV_LOAD_IMAGE_UNCHANGED);
			Mat cb = imread(cb_filename.str(), CV_LOAD_IMAGE_UNCHANGED);

			mapImg(rgb, d, rgb_d);

			cv::imshow("RGB", rgb);
			cv::imshow("RGB-D", rgb_d);

			waitKey(50);

			// Load chessboard corners from xml
			/*ostringstream mpnt_filename;
			mpnt_filename << "storage/model_rgb_world_points_" << i << ".xml";
			FileStorage fsMPnt(mpnt_filename.str(), FileStorage::READ);

			vector<Point2f> frameModelPoints;
			fsMPnt["modelPoints"] >> frameModelPoints;
			fsMPnt.release();*/

			Mat tmpPattern;
			if (!MIRROR_PATTERN) {
				tmpPattern = cb;
			}
			else {
				cv::flip(cb, tmpPattern, 1);
			}

			// Detect chessboard corners from image
			vector<Point2f> frameModelPoints;
			bool found = findChessboardCorners(tmpPattern, boardSize, frameModelPoints);
			if (!found) {
				cerr << "Could not detect chessboard corners in model file!";
			}

			processFrame(rgb, d, frameModelPoints);
		}

		calibrated = runProjectorCalibration();

		while (1) {
			if (waitKey(50) == 27) break; // wait for esc
		}
	}
}


void processFrame(Mat &rgb, Mat &d, vector<Point2f> &frameModelPoints) {

	vector<Point2f> pointBuf;
	//vector<Point2f> circleBuf;

	cout << "Taking sample...\n";
	bool found = findChessboardCorners(rgb, boardSize, pointBuf);
	
	if (found) {
		imagePoints.push_back(pointBuf);

		if (STORE_IMAGES) {
			ostringstream rgb_filename, d_filename, cb_filename;
			rgb_filename << "rgb" << worldPoints.size() << ".png";
			d_filename << "d" << worldPoints.size() << ".png";
			cb_filename << "cb" << worldPoints.size() << ".png";

			imwrite(rgb_filename.str(), rgb);
			imwrite(d_filename.str(), d);
			imwrite(cb_filename.str(), pattern);
		}

		Mat viewGrayRGB;
		cvtColor(rgb, viewGrayRGB, CV_BGRA2GRAY);
		cornerSubPix(viewGrayRGB, pointBuf, Size(11, 11),
			Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));

		drawChessboardCorners(rgb, boardSize, Mat(pointBuf), found);

		cv::imshow("RGB", rgb);
		waitKey(50);


		vector<Point3f> frameWorldPoints;

		// For each RGB corner point find according world point
		for (int i = 0; i < pointBuf.size(); i++) {
			Point2f pntRGB = pointBuf.at(i);

			Point3d depthWorldPoint = findWorldPoint(pntRGB);
			//Point3d rgbWorldPoint = depthWorldtoRgbWorld(depthWorldPoint);

			frameWorldPoints.push_back(depthWorldPoint);
		}

		char key = (char)waitKey(2000);

		worldPoints.push_back(frameWorldPoints);
		modelPoints.push_back(frameModelPoints);

		assert(frameWorldPoints.size() == frameModelPoints.size());
		assert(worldPoints.size() == modelPoints.size());

		cout << "Got " << worldPoints.size() << " samples!\n";
	}
	else {
		cout << "No chessboard was found in RGB image!";
	}
}


static bool runProjectorCalibration()
{

	cout << "Running calibration...";

	cameraMatrixProj = (Mat1d(3, 3) <<
		PROJ_WIDTH, 0, PROJ_WIDTH / 2.,
		0, PROJ_HEIGHT, PROJ_HEIGHT / 2.,
		0, 0, 1);
	distCoeffsProj = Mat::zeros(8, 1, CV_64F);

	int flags = CV_CALIB_USE_INTRINSIC_GUESS;
	//flags |= CV_CALIB_FIX_K1 | CV_CALIB_FIX_K2 | CV_CALIB_FIX_K3;
	//flags |= CV_CALIB_ZERO_TANGENT_DIST;
	//flags |= CV_CALIB_CB_FAST_CHECK;

	//THIS IS NEEDED BECAUSE IT WONT WORK OTHERWISE!!!
	vector<vector<Point3f> > allinone_obj(1);
	vector<vector<Point2f> > allinone_img(1);
	for (int i = 0; i<worldPoints.size(); ++i) {
		for (int j = 0; j<worldPoints[i].size(); j++) {
			allinone_obj[0].push_back(worldPoints[i][j]);
			allinone_img[0].push_back(modelPoints[i][j]);
		}
	}

	//Find intrinsic camera parameters
	Size imageSize = Size(PROJ_WIDTH, PROJ_HEIGHT);
	double rms;
	vector<Mat> R_vec, T_vec;
	rms = calibrateCamera(allinone_obj, allinone_img, imageSize, cameraMatrixProj, distCoeffsProj, R_vec, T_vec, flags);

	cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

	bool ok = checkRange(cameraMatrixProj) && checkRange(distCoeffsProj);

	if (ok) {
		depthToProjT = T_vec[0];
		depthToProjR = R_vec[0];

		cout << "CALIBRATED!";
		cout << "cameraMatrix = " << endl << " " << cameraMatrixProj << endl << endl;
		cout << "trans =" << depthToProjT << endl;
		cout << "rot =" << depthToProjR << endl;

		storeProjectorCalibAndPoints();
	}
	else
		cout << "CALIBRATION ERROR!" << endl;	

	return ok;
}

/*static bool runProjectorStereoCalibration(Mat& rvec, Mat& tvec, Mat& E, Mat& F)
{

	//Find extrinsic camera parameters
	Size imageSize = Size(PROJ_WIDTH, PROJ_HEIGHT);
	double rms;
	rms = stereoCalibrate(worldPoints, imagePoints, modelPoints,
		cameraMatrixRGB, distCoeffsRGB,
		cameraMatrixProj, distCoeffsProj,
		imageSize,
		rvec, tvec, E, F);

	cout << "Re-projection error reported by stereoCalibrateCamera: " << rms << endl;

	cout << "Stereo Tvec:" << tvec << endl;

	Vec3d rotEuler123;
	rotEuler123[0] = atan2(rvec.at<double>(2, 1), rvec.at<double>(2, 2)) * 180 / 3.1415;
	rotEuler123[1] = -asin(rvec.at<double>(2, 0)) * 180 / 3.1415;
	rotEuler123[2] = atan2(rvec.at<double>(1, 0), rvec.at<double>(0, 0)) * 180 / 3.1415;

	cout << "RotEuler123: " << rotEuler123 << endl;

	storeProjectorCalibAndPoints();

	//totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix,
	//	distCoeffs, reprojErrs, s.useFisheye);

	return true;
}*/


void showCalibrationPattern() {

	pattern = Mat(PROJ_HEIGHT, PROJ_WIDTH, CV_8UC1, chessboardBackgroundColor);

	int maxPatternHeight = chessboardOffsetY + squareSize * (boardSize.height + 1);
	int maxPatternWidth = chessboardOffsetX + squareSize * (boardSize.width + 1);

	if (maxPatternHeight > PROJ_HEIGHT || maxPatternWidth > PROJ_WIDTH) {
		cerr << "Chessboard pattern values are out of range!\n";
		return;
	}

	bool blackY = false;
	for (int y = chessboardOffsetY; y < maxPatternHeight; y++) {
		bool blackX = false;
		if (((y - chessboardOffsetY) % squareSize) == 0)
			blackY = !blackY;

		blackX = blackY;

		for (int x = chessboardOffsetX; x < maxPatternWidth; x++) {
			if (((x - chessboardOffsetX) % squareSize) == 0)
				blackX = !blackX;

			if (blackX)
				pattern.at<uchar>(y, x) = (uchar)0;
			else
				pattern.at<uchar>(y, x) = (uchar)chessboardBackgroundColor;
		}
	}

	namedWindow("Chessboard", CV_WINDOW_NORMAL);
	cv::moveWindow("Chessboard", 3444, 0);
	cv::setWindowProperty("Chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	imshow("Chessboard", pattern);

	/*bool blink = false;
	for (int i = 0; i < 10; i++) {
		vector<Point2f> objPoints;
		calcBoardCornerPositionsWithOffset(boardSize, squareSize, objPoints);

		for (int i = 0; i < objPoints.size(); i++) {
			Point2f point = objPoints[i];
			if(blink)
				pattern.at<uchar>(point.y, point.x) = (uchar)0;
			else
				pattern.at<uchar>(point.y, point.x) = (uchar)255;
		}

		blink = !blink;
		char key = (char)waitKey(50);
		imshow("Chessboard", pattern);
	}*/
}

Point3d depthWorldtoRgbWorld(Point3d depthWorld) {
	Mat p3d(depthWorld);
	Mat R_trans;

	p3d = rgbToDepthR.inv() * p3d - rgbToDepthT;

	double x = p3d.at<double>(0, 0);
	double y = p3d.at<double>(1, 0);
	double z = p3d.at<double>(2, 0);

	return Point3d(x, y, z);
}

Point3d depthToDepthWorld(int x, int y, float depth) {
	Point3d result;
	result.x = (float)(x - depthCX) * depth / depthFX;
	result.y = (float)(y - depthCY) * depth / depthFY;
	result.z = (float)depth;
	return result;
}

Point2i rgbWorldToRGB(const Point3d & point) {
	Point2i result;
	result.x = (int)round((point.x * rgbFX / point.z) + rgbCX);
	result.y = (int)round((point.y * rgbFY / point.z) + rgbCY);
	return result;
}

bool isInRange(int val, int max) {
	return val <= max && val >= 0;
}

void mapImg(Mat& rgb, Mat& depth, Mat& rgb_d) {
	rgb_d = Mat(depth.size(), CV_8UC4, Scalar(0));
	ushort *raw_image_ptr;

	mappedRgbPoints.clear();
	depthWorldCoords.clear();

	/*#if (!_DEBUG)
	#pragma omp parallel for
	#endif*/
	for (int y = 0; y < depth.rows; y++) {
		raw_image_ptr = depth.ptr<ushort>(y);

		for (int x = 0; x < depth.cols; x++) {
			if (raw_image_ptr[x] >= MAX_KINECT_VAL || raw_image_ptr[x] <= 0)
				continue;

			float depth_value_mm = raw_image_ptr[x];

			//if (y == 212 && x == 256)
			//	cout << "Ushort: " << raw_image_ptr[x] << " Depth in mm:" << depth_value << "\n";

			Point3d depth_world = depthToDepthWorld(x, y, depth_value_mm);
			Point3d rgb_world = depthWorldtoRgbWorld(depth_world);

			Point2i rgb_coord = rgbWorldToRGB(rgb_world);

			if (isInRange(rgb_coord.x, RGB_COLUMNS - 1) && isInRange(rgb_coord.y, RGB_ROWS - 1)) {
				rgb_d.at<Vec4b>(y, x) = rgb.at<Vec4b>(rgb_coord.y, rgb_coord.x);

				mappedRgbPoints.push_back(rgb_coord);
				depthWorldCoords.push_back(depth_world);

#if (MARK_DEPTH_POINTS)
				rgb.at<Vec4b>(rgb_coord[1], rgb_coord[0]) = Vec4b((uchar)0, (uchar)0, (uchar)255, (uchar)255);
#endif				
			}
		}
	}
}

static void calcBoardCornerPositionsWithOffset(Size boardSize, float squareSize, vector<Point2f>& corners)
{
	corners.clear();

	for (int i = 1; i < boardSize.height + 1; ++i)
		for (int j = 1; j < boardSize.width + 1; ++j)
			corners.push_back(Point2f(chessboardOffsetX + j*squareSize, chessboardOffsetY + i*squareSize));
}

static void calcBoardCornerPositionsWithOffsetMirrored(Size boardSize, float squareSize, vector<Point2f>& corners)
{
	corners.clear();

	for (int i = 1; i < boardSize.height + 1; ++i)
		for (int j = boardSize.width; j > 0; --j)
			corners.push_back(Point2f(PROJ_WIDTH - chessboardOffsetX + j*squareSize, chessboardOffsetY + i*squareSize));
}


Point3d findWorldPoint(Point2i rgbPoint) {
	/* Find nearest depth mapped rgb pixel with KD radiusSearch*/
	flann::KDTreeIndexParams indexParams;

	flann::Index kdtree(Mat(mappedRgbPoints).reshape(1), indexParams);
	vector<float> query;
	query.push_back(rgbPoint.x);
	query.push_back(rgbPoint.y);

	vector<int> indices;
	vector<float> dists;

	int radius = 6;
	int maxValues = 1;
	kdtree.radiusSearch(query, indices, dists, radius, maxValues);

	int foundIndex = indices[0];

	// Get according rgb and depth pixel

	//Point2i nbRgbPoint = mappedRgbPoints.at(foundIndex);
	//cout << "Pixel " << nbRgbPoint.x << " " << nbRgbPoint.y << " is the next with depth value!\n";

	Point3d depthWorld = depthWorldCoords.at(foundIndex);
	cout << "Found world point " << depthWorld.x << " " << depthWorld.y << " " << depthWorld.z << endl;

	return depthWorld;
}

static bool firstClick = true;
static void onMouse(int event, int x, int y, int, void*)
{
	if (event != EVENT_LBUTTONDOWN)
		return;

	cout << "Clicked on RGB: ";

	if (!LOAD_PREVIOUS_PROJ_CALIB && !calibrated) {
		cout << "Can not test projecting. Not calibrated yet..." << endl;
		return;
	}

	cout << "Selected pixel " << x << " : " << y << " pixel\n";

	Point3d depthWorldPoint = findWorldPoint(Point2i(x, y));

	//Point3d rgbWorldPoint = depthWorldtoRgbWorld(depthWorldPoint);

	cout << "Clicked on World " << depthWorldPoint.x << " " << depthWorldPoint.y << " " << depthWorldPoint.z << "!\n";
	
	vector<Point3f> points;
	points.push_back(depthWorldPoint);

	Mat projImagePoint;
	projectPoints(points, depthToProjR, depthToProjT, cameraMatrixProj, distCoeffsProj, projImagePoint);

	// mirror
	int proj_x = PROJ_WIDTH - projImagePoint.at<float>(0, 0);

	int proj_y = projImagePoint.at<float>(0, 1);

	cout << "Yay! Projected image point is at " << proj_x << " " << proj_y << endl;

	// Creates calibration pattern if not existent yet
	if(firstClick)
		showCalibrationPattern();

	Scalar color = 0;
	circle(pattern, Point2d(proj_x, proj_y), 5, color, -1, 0, 0);

	imshow("Chessboard", pattern);

	firstClick = false;
}

void storeProjectorCalibAndPoints() {
	// Store world and model points
	cout << "Storing intrinsics..." << endl;
	FileStorage fs(kinectProjectorParams, FileStorage::WRITE);
	fs << "camera_matrix" << cameraMatrixProj;
	fs << "distortion_coefficients" << distCoeffsProj;
	fs << "R" << depthToProjR;
	fs << "T" << depthToProjT;
	fs.release();

	cout << "Storing points..." << endl;
	FileStorage fsp("world_and_model_points.xml", FileStorage::WRITE);

	fsp << "worldPoints" << worldPoints;
	fsp << "modelPoints" << modelPoints;
	fsp.release();

	cout << "Done." << endl;
}


/* ------------ Calibration/projection with QR decomposition ----------------------*/


void qrCalibrate(vector<vector<Point3f>> worldPoints, vector<vector<Point2f>> modelPoints) {
	int nrPointsFrame = boardSize.width * boardSize.height;
	int nrPointsTotal = nrPointsFrame * nrFrames;

	Mat A( nrPointsTotal * 2, 11, CV_32F);
	Mat y(nrPointsTotal * 2, 1, CV_32F);
	Mat x(11, 1, CV_32F);

	for (int j = 0; j < nrFrames; j++)
		for (int i = 0; i < nrPointsFrame; i++) {
			int v = 2 * (j*nrPointsFrame + i);

			A.at<float>(v, 0) = worldPoints[j][i].x;
			A.at<float>(v, 1) = worldPoints[j][i].y;
			A.at<float>(v, 2) = worldPoints[j][i].z;
			A.at<float>(v, 3) = 1;
			A.at<float>(v, 4) = 0;
			A.at<float>(v, 5) = 0;
			A.at<float>(v, 6) = 0;
			A.at<float>(v, 7) = 0;
			A.at<float>(v, 8) = -worldPoints[j][i].x * modelPoints[j][i].x;
			A.at<float>(v, 9) = -worldPoints[j][i].y * modelPoints[j][i].x;
			A.at<float>(v, 10) = -worldPoints[j][i].z * modelPoints[j][i].x;

			A.at<float>(v + 1, 0) = 0;
			A.at<float>(v + 1, 1) = 0;
			A.at<float>(v + 1, 2) = 0;
			A.at<float>(v + 1, 3) = 0;
			A.at<float>(v + 1, 4) = worldPoints[j][i].x;
			A.at<float>(v + 1, 5) = worldPoints[j][i].y;
			A.at<float>(v + 1, 6) = worldPoints[j][i].z;
			A.at<float>(v + 1, 7) = 1;
			A.at<float>(v + 1, 8) = -worldPoints[j][i].x * modelPoints[j][i].y;
			A.at<float>(v + 1, 9) = -worldPoints[j][i].y * modelPoints[j][i].y;
			A.at<float>(v + 1, 10) = -worldPoints[j][i].z * modelPoints[j][i].y;

			y.at<float>(v, 0) = modelPoints[j][i].x;
			y.at<float>(v + 1, 0) = modelPoints[j][i].y;
		}

	cv::solve(A, y, x, DECOMP_QR);

	cout << "QR solve: x=" << x << endl;

	calibrated = true;
}

Point2f getProjectedPointQR(Mat &x, Point3f worldPoint) {
	float a = x.at<float>(0, 0)*worldPoint.x + x.at<float>(1, 0)*worldPoint.y + x.at<float>(2, 0)*worldPoint.z + x.at<float>(3, 0);
	float b = x.at<float>(4, 0)*worldPoint.x + x.at<float>(5, 0)*worldPoint.y + x.at<float>(6, 0)*worldPoint.z + x.at<float>(7, 0);
	float c = x.at<float>(8, 0)*worldPoint.x + x.at<float>(9, 0)*worldPoint.y + x.at<float>(10, 0)*worldPoint.z + 1;
	Point2f projectedPoint(a / c, b / c);
	return projectedPoint;
}