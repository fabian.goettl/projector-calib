cmake_minimum_required(VERSION 2.8)
project(KinectV2ProCam)

SET(BUILD_SHARED_LIBS ON)
find_package(OpenCV REQUIRED)
include_directories( ${OpenCV_INCLUDE_DIRS} )
MESSAGE(STATUS ${OpenCV_INCLUDE_DIRS} )

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

# kinect for windows sdk
set(KinectSDK2_FOUND OFF CACHE BOOL "Kinect 2.x SDK found")

if(WIN32)
    if(EXISTS $ENV{KINECTSDK20_DIR})
        message(STATUS KINECT_FOUND)
		message(STATUS $ENV{KINECTSDK20_DIR})
        set(KinectSDK2_FOUND ON CACHE BOOL "Kinect 2.x SDK found" FORCE)
        set(KinectSDK2_DIR $ENV{KinectSDK2_DIR} CACHE PATH "Kinect 2.x SDK path" FORCE)
        if(EXISTS $ENV{KINECTSDK20_DIR}/inc)
            set(KinectSDK2_INCLUDE_DIRS $ENV{KINECTSDK20_DIR}/inc)
			include_directories( ${KinectSDK2_INCLUDE_DIRS})
            message(STATUS KINECT_INC_FOUND)
        endif()
        if(CMAKE_CL_64)
			message(STATUS "arch 64")
			if(EXISTS $ENV{KINECTSDK20_DIR}/lib/x64/Kinect20.lib)
				set(KinectSDK2_LIBRARIES $ENV{KINECTSDK20_DIR}/lib/x64/Kinect20.lib)
				message(STATUS KINECT_LIB_FOUND at  $ENV{KINECTSDK20_DIR}/lib/x86/Kinect20.lib)
			else()
				message(FATAL_ERROR "Chosen arch is 64 but Kinect library 64 bit version not found, check that" $ENV{KINECTSDK10_DIR} " contains /lib/amd64  folder")
			endif()
		else()
			message(STATUS "arch x86")
			if(EXISTS $ENV{KINECTSDK20_DIR}/lib/x86/Kinect20.lib)
				set(KinectSDK2_LIBRARIES $ENV{KINECTSDK20_DIR}/lib/x86/Kinect20.lib)
				message(STATUS KINECT_LIB_FOUND at  $ENV{KINECTSDK20_DIR}/lib/x64/Kinect20.lib)
			else()
				message(FATAL_ERROR "Chosen arch is x86 but Kinect library 32 bit version not found, check that" $ENV{KINECTSDK10_DIR} " contains /lib/x86  folder")
			endif()
		endif()
    endif()
else()
message(KINECT_NOT_FOUND)
endif()


set(srcs simple-procam.cpp kinectv2stream.cpp)
set(hdrs)

set(link_libs ${OpenCV_LIBS}  ${KinectSDK2_LIBRARIES})


add_executable(sprocam ${srcs} ${hdrs})

target_link_libraries(sprocam
	${link_libs}
	)

